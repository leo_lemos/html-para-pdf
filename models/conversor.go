package models

type ConverterHTMLToPDFRequest struct {
	HTML             string  `json:"html"`
	PageSetup        *string `json:"pageSetup"`
	PageMarginTop    *uint   `json:"pageMarginTop"`
	PageMarginBottom *uint   `json:"pageMarginBottom"`
	PageMarginLeft   *uint   `json:"pageMarginLeft"`
	PageMarginRight  *uint   `json:"pageMarginRight"`
}
