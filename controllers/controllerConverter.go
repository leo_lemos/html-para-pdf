package controllers

import (
	"encoding/base64"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/sbsistemas/apis/html-para-pdf/env"
	"gitlab.com/sbsistemas/apis/html-para-pdf/models"
	"gitlab.com/sbsistemas/apis/html-para-pdf/multithread"
)

func ConverterHTMLParaPDF() func(c *gin.Context) {
	ambiente := env.GetEnvironment()

	return func(c *gin.Context) {
		var request *models.ConverterHTMLToPDFRequest

		if err := c.ShouldBindJSON(&request); err != nil {
			var erro string

			if ambiente == "production" {
				erro = "Erro ao converter HTML para PDF"
			} else {
				erro = err.Error()
			}

			c.JSON(400, gin.H{
				"error": erro,
			})
			return
		}

		if request == nil {
			c.JSON(500, gin.H{
				"error": "Não foi possível converter o HTML para PDF",
			})
			return
		}

		if request.HTML == "" {
			c.JSON(400, gin.H{
				"error": "O HTML entregue não pode ser vazio",
			})
			return
		}

		requestedFilename := time.Now().String() + ".pdf"

		pageSetup := request.PageSetup
		marginTop := request.PageMarginTop
		marginBottom := request.PageMarginBottom
		marginLeft := request.PageMarginLeft
		marginRight := request.PageMarginRight

		err := multithread.ConvertHTMLToPDF(request.HTML, requestedFilename, *pageSetup, multithread.PDFMarginOptions{
			MarginTop:    marginTop,
			MarginBottom: marginBottom,
			MarginLeft:   marginLeft,
			MarginRight:  marginRight,
		})

		if err != nil {
			var erro string

			if ambiente == "production" {
				erro = "Erro ao converter HTML para PDF"
			} else {
				erro = err.Error()
			}

			c.JSON(500, gin.H{
				"error": erro,
			})
			return
		}

		dir := "/tmp/" + requestedFilename
		encode, err := base64.StdEncoding.DecodeString(dir)

		if err != nil || len(encode) == 0 {
			var erro string

			if ambiente == "production" {
				erro = "Não foi possível converter o PDF para base64"
			} else {
				erro = err.Error()
			}

			c.JSON(500, gin.H{
				"error": erro,
			})
			return
		}

		c.JSON(200, gin.H{
			"data": string(encode),
		})
	}
}
