package multithread

import (
	"errors"
	"strings"

	wkhtml "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

type PDFMarginOptions struct {
	MarginTop    *uint
	MarginBottom *uint
	MarginLeft   *uint
	MarginRight  *uint
}

func ConvertHTMLToPDF(html string, filename string, pageSetup string, marginOptions PDFMarginOptions) error {
	if pageSetup == "" {
		pageSetup = "A4"
	}

	if html == "" {
		return errors.New("HTML is empty")
	}

	if filename == "" {
		return errors.New("filename is empty")
	}

	docGenerator, err := wkhtml.NewPDFGenerator()

	if err != nil {
		return err
	}

	docGenerator.Dpi.Set(300)
	docGenerator.PageSize.Set(pageSetup)

	if marginOptions.MarginTop != nil {
		docGenerator.MarginTop.Set(*marginOptions.MarginTop)
	}

	if marginOptions.MarginBottom != nil {
		docGenerator.MarginBottom.Set(*marginOptions.MarginBottom)
	}

	if marginOptions.MarginLeft != nil {
		docGenerator.MarginLeft.Set(*marginOptions.MarginLeft)
	}

	if marginOptions.MarginRight != nil {
		docGenerator.MarginRight.Set(*marginOptions.MarginRight)
	}

	readerHTML := strings.NewReader(html)
	readerPage := wkhtml.NewPageReader(readerHTML)

	docGenerator.AddPage(readerPage)

	err = docGenerator.Create()

	if err != nil {
		return err
	}

	err = docGenerator.WriteFile("/tmp/" + filename)
	return err
}
