package env

import (
	"os"

	"github.com/subosito/gotenv"
)

func LoadEnvironment() error {
	err := gotenv.Load()

	if err != nil {
		return err
	}

	return nil
}

func GetPort() string {
	port := os.Getenv("PORT")

	if port != "" {
		return port
	}

	env := os.Getenv("GO_ENV")

	if env == "production" {
		return "8080"
	}
	return "3000"
}

func GetEnvironment() string {
	env := os.Getenv("GO_ENV")

	if env == "" {
		return "production"
	}

	return env
}
