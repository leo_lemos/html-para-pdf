package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/sbsistemas/apis/html-para-pdf/controllers"
	"gitlab.com/sbsistemas/apis/html-para-pdf/env"
)

func main() {
	if err := env.LoadEnvironment(); err != nil {
		panic(err)
	}

	r := gin.Default()
	ambiente := env.GetEnvironment()

	if ambiente == "production" {
		r.Use(gin.Recovery())
		gin.SetMode(gin.ReleaseMode)
	} else {
		r.Use(gin.Logger())
		gin.SetMode(gin.DebugMode)
	}

	r.POST("/", controllers.ConverterHTMLParaPDF())

	port := env.GetPort()
	r.Run(":" + port)
}
